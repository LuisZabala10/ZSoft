﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;

namespace ZSoft.Core.Interfaces
{
    public interface IProductoServicios
    {
        Task<IEnumerable<Producto>> GetProductos();
    }
}
