﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;

namespace ZSoft.Core.Interfaces
{
    public interface IMarca
    {
        Task<IEnumerable<Marca>> GetMarcas();
    }
}
