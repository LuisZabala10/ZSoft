﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;
using ZSoft.Core.Interfaces;

namespace ZSoft.Core.Services
{
    public class CategoriaServicios : ICategoriaServicios
    {
        private readonly ICategoria _categoriaRepositorio;

        public CategoriaServicios(ICategoria categoriaRepositorio)
        {
            _categoriaRepositorio = categoriaRepositorio;
        }
        public async Task<IEnumerable<Categoria>> GetCategorias()
        {
            return await _categoriaRepositorio.GetCategorias();
        }
    }
}
