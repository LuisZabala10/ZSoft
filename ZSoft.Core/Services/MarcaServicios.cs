﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;
using ZSoft.Core.Interfaces;

namespace ZSoft.Core.Services
{
    public class MarcaServicios : IMarcaServicios
    {
        private readonly IMarca _marcaRepositorio;

        public MarcaServicios(IMarca marcaRepositorio)
        {
            _marcaRepositorio = marcaRepositorio;
        }

        public async Task<IEnumerable<Marca>> GetMarcas()
        {
            return await _marcaRepositorio.GetMarcas();
        }
    }
}
