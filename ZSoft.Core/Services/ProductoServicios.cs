﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;
using ZSoft.Core.Interfaces;

namespace ZSoft.Core.Services
{
    public class ProductoServicios : IProductoServicios
    {
        private readonly IProducto _productoRepositorio;

        public ProductoServicios(IProducto productoRepositorio)
        {
            _productoRepositorio = productoRepositorio;
        }

        public async Task<IEnumerable<Producto>> GetProductos()
        {
            return await _productoRepositorio.GetProductos();
        }
    }
}
