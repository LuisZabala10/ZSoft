﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Interfaces;
using ZSoft.UI.ViewModels;

namespace ZSoft.UI.Controllers
{
    public class ProductosController : Controller
    {
        private readonly IProductoServicios _productoServicios;

        public ProductosController(IProductoServicios productoServicios)
        {
            _productoServicios = productoServicios;
        }
        public async Task<IActionResult> Index()
        {

            List<ProductoViewModel> model = new List<ProductoViewModel>();

            var productos = await _productoServicios.GetProductos();

            foreach(var producto in productos)
            {
                model.Add(new ProductoViewModel
                {
                    Id = producto.Id,
                    Nombre = producto.Nombre,
                    PrecioCosto = producto.PrecioCosto,
                    PrecioVenta = producto.PrecioVenta,
                    Categoria = producto.Categoria.Nombre,
                    Marca = producto.Marca.Nombre
                });
            }

            return View(model);
        }
    }
}
