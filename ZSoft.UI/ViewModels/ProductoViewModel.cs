﻿namespace ZSoft.UI.ViewModels
{
    public class ProductoViewModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public decimal PrecioCosto { get; set; }
        public decimal PrecioVenta { get; set; }
        public string Categoria { get; set; }
        public string Marca { get; set; }
    }
}
