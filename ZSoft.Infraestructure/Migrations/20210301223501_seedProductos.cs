﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZSoft.Infraestructure.Migrations
{
    public partial class seedProductos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Productos(Nombre,PrecioCosto,PrecioVenta,FechaCreacion,CategoriaId,MarcaId)" +
                "VALUES('Tenis Addidas Size 10','2200','2800','03/01/2021',2,1)");

            migrationBuilder.Sql("INSERT INTO Productos(Nombre,PrecioCosto,PrecioVenta,FechaCreacion,CategoriaId,MarcaId)" +
                "VALUES('Cerveza Presidente Mediana','90','140','03/01/2021',1,4)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
