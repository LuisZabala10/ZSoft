﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZSoft.Infraestructure.Migrations
{
    public partial class seedMarcas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Marcas(Nombre) VALUES('Addidas')");
            migrationBuilder.Sql("INSERT INTO Marcas(Nombre) VALUES('Generico')");
            migrationBuilder.Sql("INSERT INTO Marcas(Nombre) VALUES('Nike')");
            migrationBuilder.Sql("INSERT INTO Marcas(Nombre) VALUES('Presidente')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
