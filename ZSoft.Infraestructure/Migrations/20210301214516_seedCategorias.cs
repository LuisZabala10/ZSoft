﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ZSoft.Infraestructure.Migrations
{
    public partial class seedCategorias : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Categorias(Nombre) VALUES('Bebidas')");
            migrationBuilder.Sql("INSERT INTO Categorias(Nombre) VALUES('Calzados')");
            migrationBuilder.Sql("INSERT INTO Categorias(Nombre) VALUES('Ropa')");
            migrationBuilder.Sql("INSERT INTO Categorias(Nombre) VALUES('Prendas')");
            migrationBuilder.Sql("INSERT INTO Categorias(Nombre) VALUES('Accesorios')");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
