﻿using Microsoft.EntityFrameworkCore;
using ZSoft.Core.Entities;

namespace ZSoft.Infraestructure.Data
{
    public class ZsoftContext:DbContext
    {
        public ZsoftContext( DbContextOptions<ZsoftContext> options):base(options)
        {
        }

        public DbSet<Producto> Productos { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Marca> Marcas { get; set; }
    }
}
