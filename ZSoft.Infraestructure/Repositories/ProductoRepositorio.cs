﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;
using ZSoft.Core.Interfaces;
using ZSoft.Infraestructure.Data;

namespace ZSoft.Infraestructure.Repositories
{
    public class ProductoRepositorio : IProducto
    {
        private readonly ZsoftContext _context;

        public ProductoRepositorio(ZsoftContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Producto>> GetProductos()
        {
            return await _context.Productos.
                Include(p => p.Categoria).
                Include(p => p.Marca).
                ToListAsync();
        }
    }
}
