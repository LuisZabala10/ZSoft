﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;
using ZSoft.Core.Interfaces;
using ZSoft.Infraestructure.Data;

namespace ZSoft.Infraestructure.Repositories
{
    public class MarcaRepositorio : IMarca
    {
        private readonly ZsoftContext _context;

        public MarcaRepositorio(ZsoftContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Marca>> GetMarcas()
        {
            return await _context.Marcas.ToListAsync();
        }
    }
}
