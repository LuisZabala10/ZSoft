﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZSoft.Core.Entities;
using ZSoft.Core.Interfaces;
using ZSoft.Infraestructure.Data;

namespace ZSoft.Infraestructure.Repositories
{
    public class CategoriaRepositorio : ICategoria
    {
        private readonly ZsoftContext _context;

        public CategoriaRepositorio(ZsoftContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Categoria>> GetCategorias()
        {
            return await _context.Categorias.ToListAsync();
        }
    }
}
